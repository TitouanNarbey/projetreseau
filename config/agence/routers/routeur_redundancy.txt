###CONFIG routeur###
Display name -> Routeur redondance - Agence
Hostname -> routerRedondanceAgence

###CONFIG interface###
fastEthernet0/0
	10.3.5.8
	255.255.255.0

###CONFIG HSRP###
en
conf t
int fa0/0
standby 1 ip 10.3.5.6
standby 1 preempt
standby 1 track fa0/0
no shutdown
ex
ex
show standby fa0/0