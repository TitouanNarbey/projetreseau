###CONFIG switch###
Display name -> Switch principal - P�le Commercial
Hostname -> switchPoleCommercial

### Passer toutes les interfaces en trunk  ###
en
conf t
int range fa0/1-2
switchport mode trunk


### Setup le client VTP ###
en
conf t
vtp domain vergis.com
vtp password cisco
vtp version 2
vtp mode client

show vtp status


### Setup les ports pour VLAN ###
en
conf t
int range fa0/{ports PC}  ---> dans notre cas int range fa0/3-24
switchport access vlan {vlan name/number} ---> dans notre cas switchport access vlan 24

