###CONFIG switch###
Display name -> Switch VLAN24
Hostname -> switchVLAN24

### Passer toutes les interfaces en trunk  ###
en
conf t
int range fa0/2-3
switchport trunk encapsulation dot1q
switchport mode trunk

### Setup le serveur VTP ###
en
conf t
vtp domain vergis.com
vtp password cisco
vtp version 2
vtp mode server
ex
show interface trunk


### Ajouter VLAN au serveur VTP ###
en
conf t
vlan 24
name VLANcommercial